---
title: "Save the date!"
date: "2023-07-05"
---
Der [0x90.space](https://0x90.space/) lädt ein zum "Quellcode" vom 16.9 - 17.9.2023 im [Heizhaus Nürnberg](https://heizhaus.org/) ([Wandererstr. 89c, 90431 Nürnberg](https://osm.org/go/0D6zlBi~x-?way=37685652)).
Das Ziel ist Menschen aus verschiedenen Hintergründen zusammenzubringen, um voneinander zu lernen.
Eine Gelegenheit Erfahrungen auszutauschen und kreative Dinge mit Technik (oder auch ohne) anzustellen.

Wir haben einen Vortrags-/Workshopsraum in den ca 20 Leute passen und ein größeres Hackcenter mit Tischen und Stühlen für 50-100 Leute in dem ihr sitzen, reden und eure Projekte zeigen könnt.
Wenn ihr einen Vortrag oder Workshop halten wollt, [meldet euch bis zum 26.08.2023 00:00 CEST](https://cryptpad.fr/form/#/2/form/view/KR2e9HvcShTc4RWnd+81jEEey8djEfu3f1dHROAO490/).
Wir wollen nicht nur über Technik und Computer reden und freuen uns auch über künstlerische oder gesellschaftspolitische Beiträge.

Es gibt ein [allgemeines Anmeldeformular](https://cryptpad.fr/form/#/2/form/view/qv48AVyeLO4-l9uIuTOeNdM7sGFR-zfTilvA8sX3-BY/), um die Anzahl der Leute und die Menge an Essen abzuschätzen zu können.
Ihr könnt auch gerne ohne Anmeldung vorbeikommen, aber esst dann nicht so viel ... ;)
Vor Ort wird es eine Spendenkasse geben.

Wir freuen uns auf Euch und eure Beiträge und falls ihr Fragen oder Anregungen habt, schreibt gerne an [people@schleuder.0x90.space](mailto:people@schleuder.0x90.space) oder kommt in unseren [Matrix-Channel](https://matrix.to/#/#quellcode:chat.heizhaus.org)!
