---
title: "Program and Rooms"
date: 2023-07-26T09:58:25+02:00
---

## Site plan

![Site plan](/posts/Lageplan.png)

In addition to our program in the lecture/workshop room, we will also have a hack center (aka a large room with tables and chairs) where you can sit, program and do mini-workshops without registration.

## Program 

- from 10 a.m. there is eternal breakfast in the "Brücke" (room in front of the lecture/workshop room)

Please check out the program at the [German page](/posts/program/).
There will be talks held in english, which are marked with [en].
