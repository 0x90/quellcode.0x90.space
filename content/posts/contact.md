---
title: "Kontakt"
date: 2023-07-26T10:28:10+02:00
---

Ihr erreicht uns über:

- Email: [people@schleuder.0x90.space](mailto:people@schleuder.0x90.space)
- Matrix: [https://matrix.to/#/#quellcode:chat.heizhaus.org](https://matrix.to/#/#quellcode:chat.heizhaus.org)
- Impressum: [hier](https://0x90.space/impressum/)
