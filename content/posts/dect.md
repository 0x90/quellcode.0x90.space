---
title: "DECT"
date: 2023-09-15T22:05:00+02:00
---

## TL;DR

Es gibt auf dem Quellcode ein eignes Telefonie-Netz mit DECT und SIP.  
(Unabhängig von Eventphone und µPOC, also ggf. mit anderen Nummern und User-Accounts!)  
Bringt eure DECT-Mobilteile mit! :)

## Wie benutze ich das?

### Telefonbuch

Ein aktuelles Telefonbuch findet ihr unter https://dect.site/.

### Dial-in aus dem öffentlichen Telefonnetz

Das Event-Telefonnetz ist auch von außen erreichbar.

**+49 941 38 3388 <Durchwahl>**

z.B. +49 941 38 3388 2000 für die POC-Hotline.

Die Dial-in-Nummer ist eine normale Nummer aus dem Ortsnetz Regensburg (Vorwahl 0941), ist also zu eurem jeweiligen Festnetzpreis erreichbar.

### Eigenes DECT-/SIP-Gerät anmelden

Unser System unterstützt diesmal Selbstregistrierung für eure DECT-Geräte (allerdings noch etwas experimentell):

- Wählt auf eurem Gerät die Anmeldung an einer neuen Basis aus (wie genau die Menüoption heißt, hängt von eurem Gerät ab)
- Gebt als Basis-PIN `0000` ein
- Registriert euch auf https://dect.site/ und legt dort eine Nummer an
- Ruft mit eurem DECT-Gerät das Anmelde-"Token" an, das https://dect.site/ zu eurer Nummer anzeigt
- Der Anruf sollte sich sofort von selbst beenden
- Nach wenigen Sekunden ist eurem Gerät eure Telefonnummer zugewiesen und ihr könnt lostelefonieren

Bei Fragen oder Problem kommt gerne auf uns zu — siehe nächster Punkt. :)

## Wem kann ich Fragen dazu stellen?

Ein paar Menschen haben sich zur ["Arbeitsgruppe DECT-Experimente" (ADE)](https://dect.network/) zusammengefunden und basteln Telefonie für kleinere Events (zum Beispiel das VVoid.Camp, die IGER, das Quellcode, …), auf denen Eventphone und µPOC nicht verfügbar sind.  
Ihr erreicht uns unter `ADE [at] dect.network` und während des Events unter der DECT-Nummer **2000** (aus dem öffentlichen Telefonnetz zum Ortstarif unter [0941 383388 2000](tel:+499413833882000)).

## Wer? Was? Warum?

- **Wer?**  
  Die "Arbeitsgruppe DECT-Experimente"! :)  
  Wir sind nicht Eventphone, und auch nicht das µPOC, aber haben von beiden viel Inspiration und Beratung erhalten.
- **Was?**  
  Ein kleines DECT-Netz (selbe Hardware wie Eventphone, aber bisher weniger fancy Automatisierung). Je nach verfügbarer Zeit auch noch SIP-Konnektivität und Dial-in aus dem öffentlichen Telefonnetz.
- **Warum?**  
  Aus Experimentierfreude, Spaß am Gerät, und weil wir finden dass unkomplizierte lokale Telefonie Chaos-Events noch näher zusammenbringt.

Die Idee zu einem eigenen DECT-Setup — inspiriert von Eventphone, aber selbst nachgebaut — entstand kurz vor dem ersten VVoidCamp (2020). Dort findet ihr auch ein bisschen mehr zur Vorgeschichte: https://vviki.vvoid.camp/dect

Mittlerweile basteln mehrere Leute an unterschiedlichen Versionen der DECT-Experimente und wir bemühen uns, dort ein bisschen Telefonie hinzubringen, wo Eventphone und µPOC gerade nicht sein können.
