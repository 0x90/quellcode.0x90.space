---
title: "Anmeldung"
date: 2023-07-26T10:22:25+02:00
---

Damit wir ungefähr abschätzen können wie viele Leute kommen und wie viel Essen wir vorbereiten müssen, könnt ihr euch in diesem Formular anmelden:

[Anmeldeformular](https://cryptpad.fr/form/#/2/form/view/qv48AVyeLO4-l9uIuTOeNdM7sGFR-zfTilvA8sX3-BY/)

Ihr könnt natürlich auch ohne Anmeldung vorbeikommen, esst dann halt nicht so viel ;) 
