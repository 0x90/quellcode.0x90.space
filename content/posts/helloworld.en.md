---
title: "Save the date!"
date: "2023-07-05"
---
The [0x90.space](https://0x90.space/) is inviting to the "Quellcode" at the [Heizhaus Nürnberg](https://heizhaus.org/) ([Wandererstr. 89c, 90431 Nürnberg](https://osm.org/go/0D6zlBi~x-?way=37685652)) from 9/16 - 9/17/2023.
The goal is to bring people from different backgrounds together to learn from each other.
An opportunity to share experiences and to be creative with technology (or without).

We have a talk/workshop room that fits about 20 people and a larger hackcenter with tables and chairs for 50-100 people where you can sit, talk and show your projects.
If you want to give a talk or workshop, [sign up by 08/26/2023 00:00 CEST](https://cryptpad.fr/form/#/2/form/view/KR2e9HvcShTc4RWnd+81jEEey8djEfu3f1dHROAO490/).
We don't just want to talk about technology and computers and welcome artistic or socio-political contributions as well.

There is a [general registration form](https://cryptpad.fr/form/#/2/form/view/qv48AVyeLO4-l9uIuTOeNdM7sGFR-zfTilvA8sX3-BY/) to estimate the number of people and the amount of food.
You are also welcome to come without registration, but don't eat so much then ... ;)
There will be a donation box on site.

We are looking forward to you and your contributions and if you have any questions or suggestions, feel free to write to [people@schleuder.0x90.space](mailto:people@schleuder.0x90.space) or join our [Matrix channel](https://matrix.to/#/#quellcode:chat.heizhaus.org)!
