---
title: "FAQ"
ShowToc: True
date: 2023-07-26T10:28:10+02:00
---

### Wo kann ich übernachten?

Wir haben eine [Unterkunftsbörse](https://cryptpad.fr/code/#/2/code/edit/tObkSeg6EaOLiLNwUxsU5flt/) in dem ihr Unterkünfte anbieten und suchen könnt

### Wie kann ich mithelfen? (Schichtplan)

Wir haben einen [Schichtplan](https://cryptpad.fr/sheet/#/2/sheet/edit/o00OoL20ICKVtA1hL+KbAZyp/) und wir suchen noch viele Helfer\*innen :)

### Wie komme ich zum Heizhaus?
Adresse:
Heizhaus, Wandererstraße 89, 90431 Nürnberg

Mit der U-Bahn:
Das Heizhaus ist an der U-Bahnstation Eberhardshof. Wenn du da ankommst, am besten in Richtung Netto (sieht man in der Ferne) rausgehen aus der Station und dann rechts daran vorbei.
Wenn ihr den großen Quelleturm seht, dann ist das Heizhaus direkt darunter.

### Gibt es für das Event ein Corona-Konzept?

Selbstverständlich.
Es ist auch schön kurz:

1. Im Workshop-Raum herrscht Maskenpflicht,
   weil wir ihn nicht lüften können.
   Dadurch dass alle anderen Masken tragen,
   muss es dann die Speaker:in nicht tun.
   Wir haben einige Masken gekauft,
   für die die nicht eh eine dabei haben.
2. Im Hackcenter haben wir tagsüber zwei Rolltore weit geöffnet.
   Wenn wir sie aus Sicherheitsgründen schließen müssen,
   lüften wir stündlich.
   Zwischen 22:00 und 06:00 Uhr müssen wir sie leider schließen.

### Gibt es für das Event ein Awareness-Konzept?

Selbstverständlich,
du findest es [hier auf dieser Seite](../awareness).
Es ist angelehnt an [das CCC-Awareness-Konzept](https://help.ccc.de/awareness/).

Wir suchen auch noch Menschen,
die sich vorstellen können,
Awareness-Schichten zu übernehmen;
kommt gerne Samstag um 11:00 Uhr
zum Awareness-Treffen vor den Rolltoren,
oder meldet euch bei awareness@schleuder.0x90.space.

### Ich habe ein Problem, an wen kann ich mich melden?

Am schnellsten erreichst du uns über den Matrix-Kanal: https://matrix.to/#/#quellcode:chat.heizhaus.org

Vor Ort wird es eine verantwortliche Person ("laufendes Infodesk") geben, die du an einer Warnweste o.ä. erkennst. An die kannst du dich jederzeit wenden.
