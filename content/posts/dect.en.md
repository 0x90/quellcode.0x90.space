---
title: "DECT"
date: 2023-09-15T22:05:00+02:00
---

## TL;DR

We are operating our own small telephony network with DECT and SIP connectivity during Quellcode.  
(This is independent of Eventphone and µPOC, so will have different numbers and user accounts!)  
Bring your DECT handsets! :)

## How do I use this?

### Phone book

You can find an up-to-date phone book at https://dect.site/.

### Dial-in from the public telephone network

The event telephone network can also be reached from outside.

**+49 941 38 3388 <extension>**

e.g. +49 941 38 3388 2000 for the POC hotline.

The dial-in number is a regular number from the Regensburg local network (area code 0941), so it can be reached at your respective landline rate.

### Register your own DECT/SIP device

This time our system supports self-registration for your DECT devices (although this is still somewhat experimental):

- Select the registration with a new base on your device (the exact name of the menu option depends on your device).
- Enter '0000' as the base PIN.
- Register at https://dect.site/ and create a number there.
- Call the registration "token" (which is displayed next to your number on https://dect.site/) with your DECT device.
- The call should end by itself immediately.
- After a few seconds your phone number is assigned to your device and you can start calling.

If you have any questions or problems, please contact us - see next point. :)

## Whom can I ask questions about this?

A few people got together to form the ["Arbeitsgruppe DECT-Experimente" (ADE)](https://dect.network/) and are tinkering with telephony for smaller events (for example VVoid.Camp, IGER, Quellcode, …) where Eventphone and µPOC are not available.  
You can reach us at `ADE [at] dect.network` and during the event at the DECT number **2000** (from the public telephone network at local rates at [0941 383388 2000](tel:+499413833882000)).

## Who? What? Why?

- **Who?**  
  The "Arbeitsgruppe DECT-Experimente" ("DECT Experiments Working Group")! :)  
  We are not Eventphone or the µPOC, but have received a lot of inspiration and advice from both.
- **What?**  
  A small DECT network (same hardware as Eventphone, but less fancy automation so far). Depending on time available, also SIP connectivity and dial-in from the public telephone network.
- Why?  
  For the joy of experimenting, having fun with the device, and because we find that uncomplicated local telephony brings chaos events even closer together.

The idea for our own DECT setup — inspired by Eventphone, but built by ourselves — was born shortly before the first VVoidCamp (in 2020). There you can also find a bit more about our history: https://vviki.vvoid.camp/dect

Since then, several people have been working on different versions of the DECT experiments and we are trying to bring a bit of telephony to places where Eventphone and µPOC cannot be at the moment.
