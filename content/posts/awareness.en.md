*This page was machine-translated*

# Awareness

You can contact the awareness team if you have experienced discrimination or harassment, 
want to deal with conflicts or emotions or simply need someone to talk to. The team members will listen to you
and, if necessary, consider with you what possible courses of action are available. 

## Contact

* Phone: +4915510469518
* Email: awareness@schleuder.0x90.space
* DECT: 113

To write to us by encrypted email, there is [this PGP key](/assets/awareness@schleuder.0x90.space.asc) with the following fingerprint:

```
E928 D522 45B2 8FDA 88EF 93F9 513D F0F2 8DB4 FD8B
```
On-site, we respond to calls via phone or DECT the fastest.
Outside of events, you can reach us by email, however, our
capacities are limited there.

## About us

At a Chaos Event many people will come together. These have
different needs and are in their acting, feeling and thinking different and unique.

Considerate behavior among each other and the respect of personal boundaries
should be guaranteed course, but even at the event, situations can arise in which people
situations may arise at the event in which people are belittled or discriminated against.
Harassment, sexism, racism, homophobia, bi- and transphobia,
Ableism (discrimination against disabled people),
anti-Semitism, anti-gypsyism and classism (discrimination against poor or socioeconomically
disadvantaged people) are not okay, even if they happen
unintentionally.

## What is Awareness?

Awareness work means sensitizing people to pay attention to themselves and others - especially to those who do not conform to social expectations. Discrimination and violations of personal boundaries are often unnoticed by the perpetrators, but very well known to the victims. Since the needs of those affected often remain invisible, we want to give them special support in taking these needs seriously and asserting them. Even what others may call "supposed trivialities" or try to explain away can hurt and affect and are therefore worth addressing.

An important part of awareness is recognizing who has the right to define a transgression: Whether their own boundaries have been crossed is always decided exclusively by the person affected. Otherwise, the majority society often assumes the right to decide who is discriminated against and who is not. It is important to us that people who are otherwise not heard or taken seriously are also noticed.

For us, this means: on the one hand, we support those affected to process situations they have experienced themselves. We offer a place for confidential discussions. We listen and, if necessary, help them to develop ways of dealing with the situation and thus strengthen their ability to act.

On the other hand, if requested, we communicate with the persons from whom discrimination and transgressive behavior originates or originated. The goal here is the aforementioned awareness: to generate understanding and awareness of how and why one's own behavior has violated another person's boundaries. We confront the assaultive or violating behavior, not the person. The goal is to take responsibility for one's own behavior.

When dealing with conflict, we are also available for moderation or accompaniment.

## How we work

The awareness team is there to help you especially when:

- your personal boundaries have been crossed,
- you have been harassed or discriminated against,
- you witness or experience (personal or structural) discrimination,
- you feel uncomfortable and need a person to talk to.

When you contact us, we usually first ask you what happened and what you need right now.
Maybe you already have concrete ideas about what
we can do. Depending on what you want, we can, for example:

- come to you in conflict situations at the event and mediate.
- offer a quiet space where you can withdraw (alone or with us) and process what you have experienced.
- contact friends and acquaintances.
- try to clarify a stressful situation (as far as possible) (e.g. by trying to explain to transgressors why their actions were transgressive).
- try to find out together with you what could help you best.


You always decide for yourselves what is the best course of action for you - we will
not do anything against your will. It's okay if you don't want to talk about something,
if you don't want to or can't talk about. Every reaction, whether sadness, anger, despair,
speechlessness, ... is okay. 

Disclaimer: The awareness team consists of people who are sensitized to awareness and have
and have experience in different areas. We
support you as good as we can. However, we would like to explicitly point out
that we do not have a therapeutic education.

Here's to a wonderful chaos event! Take care of yourselves and each other.
Your Awareness Team

## Participate

We are still looking for people who can imagine to take over during the Quellcode
awareness shifts during the source code. Please contact us via email at
awareness@schleuder.0x90.space if you are interested.

What we would like to see from those interested:

- Self-awareness and reflection (you know yourself and your limits).
- Self-organization and reliability
- Ability to work in a team and to deal with conflicts
- Empathy
- Willingness to learn
- Openness and the ability to listen
- Sensitivity to discrimination including recognition of structural discrimination

We look forward to getting to know you.
