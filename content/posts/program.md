---
title: "Programm und Räume"
date: 2023-07-26T09:58:25+02:00
---

## Lageplan

![Lageplan](/posts/Lageplan.png)

Neben unserem Programm im Vortrags/Workshopraum werden wir auch ein Hackcenter (aka einen großen Raum mit Tischen und Stühlen) haben, auf denen man sitzen, programmieren und Mini-Workshops ohne Anmeldung geben kann. 

- ab 10 Uhr gibt es ewiges Frühstück in der "Brücke" (Raum vor dem Vortrags/Workshopraum)

## Programm Samstag 16.09.23

### Vortrags/Workshopraum:
- [10:00-10:15	Welcome speech (panda) [de/en]](#1000-1015welcome-speech-panda)
- [10:30-11:15	Eine Tour durch OpenBSD (vmann) [de]](#1030-1115eine-tour-durch-openbsd-vmann)
- [11:30-12:15	Linux Kernel Entwicklung (tammi) [de]](#1130-1215linux-kernel-entwicklung-tammi)
- 12:15-13:30	Pause / Umbau zu Workshopraum
- [13:30-15:30	Hands-on workshop: how to setup a mail server for anonymous Delta Chat accounts (missytake) [de/en]](#1330-1530hands-on-workshop-how-to-setup-a-mail-server-for-anonymous-delta-chat-accounts-missytake)
- [16:00-17:45 How to Anfragen nach dem Informationsfreiheitsgesetz/Umweltinformationsgesetz/u.ä. (frodo) [de]](#1600-1745-how-to-anfragen-nach-dem-informationsfreiheitsgesetzumweltinformationsgesetzu%C3%A4-frodo)
- [18:00-20:00	Abendessen [all]](#1800-2000abendessen)
- [20:00-21:30	Konzert (das_synthikat) [all]](#2000-2130konzert--das_synthikat)
- [21:30-23:00	Konzert (Modulandi) [all]](#2130-2300konzert--modulandi)
	
### Hackcenter
- [>= 10:00 Plant Station: Plant Swap and Propagation (panda) [de/en]](#plant-station-plant-swap-and-propagation-panda)
- [13:30-15:00 Workshop@Plant Station: Let's touch grass - How to house plant propagation and care (panda) [de/en]](#workshopplant-station-lets-touch-grass---how-to-house-plant-propagation-and-care-panda)

## Programm Sonntag 17.09.2023

### Vortrags/Workshopraum
- [10:00-11:45	Haecksen-Frühstück (e33a) [de/en]](#1000-1145haecksen-frühstück-e33a)
- [12:00-13:30	Pixel-Workshop - Zeichne deinen eigenen Pixel-Art-Avatar! (Mullana) [de/en]](#1200-1330pixel-workshop---zeichne-deinen-eigenen-pixel-art-avatar-mullana)
- [14:00-15:45	dezentrale Tools für Aktivismus & Organisierung (missytake) [de]](#1400-1545dezentrale-tools-für-aktivismus--organisierung-missytake)
- ~~16:00-16:45	Einführung in die Strömungssimulation mit OpenSource (OpenFOAM)~~
- ~~16:00-16:20  Was ist das Open Build Service und was hat es mit Kiwis zu tun?~~
- [16:30-17:00	Lightning talks (mpk) [de/en]](#1630-1700lightning-talks-mpk)
- [17:00-17:45	Powerpoint Karaoke (mpk) [de/en]](#1700-1745powerpoint-karaoke-mpk)
- [18:00-18:15	End speech [de/en]](#1800-1815end-speech)
- 18:15-00:00	Abbau / Cleanup

### Hackcenter
- [>= 10:00 Plant Station: Plant Swap and Propagation (panda) [de/en]](#plant-station-plant-swap-and-propagation-panda)

----------
----------

## Details Samstag 16.09.2023
### 10:00-10:15	Welcome speech (panda)
Begrüßung und Einführung

[en]
Welcome and Introduction

### 10:30-11:15	Eine Tour durch OpenBSD (vmann)

### 11:30-12:15	Linux Kernel Entwicklung (tammi)
Schon immer mal gefragt wie der Linux-Kernel funktioniert? Ich werde versuchen euch zu zeigen, wie ihr euch mit diesem Ungetüm beschäftigen könnt.

Klar der Kernel ist hauptsächlich in C geschrieben und es wird ein bisschen Rust-Code hinzukommen. Doch Kernel-Code ist doch ziemlich anders als normaler Userspace-Code. Der Kernel ist sehr komplex, deshalb zeige ich euch nach einer Übersicht, wie man sich auf einen Teilbereich fokussiert. 

### 13:30-15:30	Hands-on workshop: how to setup a mail server for anonymous Delta Chat accounts (missytake)
In 2023, managing a mail server doesn't need to be hard. With mailcow, you get a one-in-all solution with a nice web interface. Alongside mailcow, we will install mailadm, a tool for creating anonymous Delta Chat accounts through simply scanning a QR code. 

This workshop is based on this guide: https://delta.chat/en/serverguide 

We bring servers and domains for trying it out, you bring some basic command line skills.

### 16:00-17:45 How to Anfragen nach dem Informationsfreiheitsgesetz/Umweltinformationsgesetz/u.ä. (frodo)
In diesem Workshop geht es darum, wie eins:

- die Plattform fragdenstaat.de benutzt
- die Plattform datenschmutz.de benutzt
- die gesetzlichen Grundlagen für Datenanfragen an den Staat
- die Frage, was für Infos wann nützlich sein könnten/wann sich Anfragen lohnen
- Tipps und Tricks rund um Datenanfragen 

### 18:00-20:00	Abendessen
VAPCA - VeganAnarchoPunkCooking Action

Nices veganes Essen. 

[en] Nice vegan food.

### 20:00-21:30	Konzert  (das_synthikat)
https://das-synthikat.net/

We are 2 cyberpunks who like to record lo-fi music and try to make the best out of this dystopia.

### 21:30-23:00	Konzert  (Modulandi)

8bit tek.

----------
----------

## Details Sonntag 17.09.2023

### 10:00-11:45	Haecksen-Frühstück (e33a)
https://wiki.haecksen.org/books/treffen-community/page/haecksenfruhstuck 

Die Haecksen sind eine Gruppe aus Hacker\*innen, die sich als Frauen verstehen. Bei diesem Frühstück könnt ihr uns kennenlernen und ggf. Mitglied der Haecksen werden. Die Haecksen treffen sich hauptsächlich online, aber sind dabei eine Nürnberger Lokalgruppe zu gründen. 

[en] The Haecksen are a group of hackers who see themselves as women. At this breakfast you can get to know us and possibly become a member of the Haecksen. The Haecksen mainly meet online, but are in the process of starting a Nuremberg local group.

### 12:00-13:30	Pixel-Workshop - Zeichne deinen eigenen Pixel-Art-Avatar! (Mullana)
Bitte Zeichengerät mitbringen. Tablet mit Stift ist toll, aber Laptop mit Maus geht auch. Touchpad ist nur für die ganz Harten. Ich werde verschiedene Programme zeigen, kann an freier Software aber Krita empfehlen.

[en]
Please bring drawing equipment. A tablet with a pen is great, but a laptop with a mouse works too. Touchpad is for the die-hards only. I will show various programs, but can recommend Krita for free software.


### 14:00-15:45	dezentrale Tools für Aktivismus & Organisierung (missytake)
Tech-Monopole kontrollieren heute fast die ganze digitale und nicht-digitale Welt. Um dieser Dystopie etwas entgegenzustellen, müssen wir uns organisieren, ohne Leute auszuschließen. Das geht auch ohne Google, Facebook, und Dropbox - doch was sind die Alternativen?


### 16:30-17:00	Lightning talks (mpk)
Halte spontan deinen Talk über etwas das dich begeistert in 1-10 Minuten!

Anmeldung online: https://cryptpad.fr/code/#/2/code/edit/qQGvYMCMM5QqZ3LkUm+8dalQ/

Vor Ort wird auch irgendwo ein Zettel ausliegen für offline Anmeldungen :) 

Anmeldeschluss: 5 Minuten vorher 

[en]
Spontaneously hold your talk about something that excites you in 1-10 minutes!

Registration online: https://cryptpad.fr/code/#/2/code/edit/qQGvYMCMM5QqZ3LkUm+8dalQ/

There will also be a note on site somewhere for offline registrations :)

Registration deadline: 5 minutes before


### 17:00-17:45	Powerpoint Karaoke (mpk)
Was ist [Powerpoint Karaoke](https://de.wikipedia.org/wiki/Powerpoint-Karaoke)?

Zitat Wikipedia
"Das PowerPoint-Karaoke ist ein Ableger des klassischen Karaoke, bei dem die Teilnehmer keine Liedtexte nachsingen, sondern aus dem Stegreif einen Vortrag zu ihnen vorher nicht bekannten, zufällig ausgewählten Folien halten. Diese können entweder eigens erstellt werden, oder man verwendet Folien, die man im Internet gefunden hat. Es ist ein rhetorisches, präsentatorisches Trainingsspiel mit Unterhaltungscharakter. Teilweise wird es mittlerweile auch als Improtheater- oder Theatersport-Spiel verwendet. "

[en]
What is [Powerpoint Karaoke](https://en.wikipedia.org/wiki/Powerpoint-Karaoke)?

Quoting Wikipedia
"The PowerPoint karaoke is an offshoot of the classic karaoke, in which the participants do not sing lyrics, but give an impromptu presentation of previously unknown, randomly selected slides. These can either be created specially, or slides that found on the Internet. It is a rhetorical, presentational training game with an entertainment character. It is now also used as an improv or theater sport game."


### 18:00-18:15	End speech
Ein paar Abschlussworte

[en]
A few closing words


## Details other
### Plant Station: Plant Swap and Propagation (panda)
both days \>= 10:00 @ Hackcenter

Bring your plants and swap them!

It does not matter which size (e.g. one succulent leaf, one cutting, one big monstera, all fine).
Just put it there and take another plant or take a cutting of another plant sitting there.

We will have some pots and substrates there to try out.

We will also have some liquid plant feed concentrate that you can dilute, mix and take home (I accidentally bought more than I could ever use, SO TAKE IT, ITS FREE).
Please bring some kind of bottle to fill the fertilizer in.

If you have any plant or IT security-related questions, I will be there to answer them :)

### Workshop@Plant Station: Let's touch grass - How to house plant propagation and care (panda)
Saturday 13:30-15:00

- You want to get started with plants and you are overwhelmed?
- You want to build an automated plant watering system, but have no plant to build it for?
- Your plants just always die and you don't know why?
- You want to stick it to big plant* and get free cuttings?

I got you baby.

I will teach you the basics in house plant care and propagation with different types of plants.
I will bring plants, substrate and scissors and you will need to find a way to take your free cuttings back home.

\*you know, because your cuttings will be small plants hehe
