# Awareness

Ans Awareness-Team kannst du dich wenden, wenn du Diskriminierung oder
Belästigung erfahren hast, Konflikte oder Emotionen be- oder verarbeiten
möchtest oder einfach wen zum Reden brauchst. Die Teammitglieder hören dir zu
und überlegen bei Bedarf mit dir, welche Handlungsmöglichkeiten bestehen. 

## Kontakt

* Phone: +4915510469518
* Email: awareness@schleuder.0x90.space
* DECT: 113

Um uns per verschlüsselter E-Mail zu schreiben, gibt es [diesen PGP-Key](/assets/awareness@schleuder.0x90.space.asc) mit folgendem Fingerprint:

```
E928 D522 45B2 8FDA 88EF 93F9 513D F0F2 8DB4 FD8B
```

Vor Ort reagieren wir auf Anrufe via Telefon oder DECT am schnellsten.
Außerhalb von Veranstaltungen erreicht ihr uns per Email. Da sind unsere
Kapazitäten allerdings begrenzt.

## Über uns

Auf einem Chaos Event werden viele Menschen zusammenkommen. Diese haben
allesamt verschiedene Bedürfnisse und sind in ihrem Handeln, Fühlen und Denken
unterschiedlich und einmalig.

Rücksichtsvolles Verhalten untereinander und das Respektieren persönlicher
Grenzen sollten selbstverständlich sein, doch auch auf dem Event kann es zu
Situationen kommen, in denen Menschen durch Worte oder Taten herabgewürdigt
oder diskriminiert werden. Belästigungen, Sexismus, Rassismus, Homo-, Bi- und
Transfeindlichkeit, Ableismus (Diskriminierung behinderter Menschen),
Antisemitismus, Antiziganismus und Klassismus (Diskriminierung armer bzw.
sozioökonomisch benachteiligter Menschen) sind auch dann nicht okay, wenn sie
unbeabsichtigt geschehen.

## Was ist Awareness?

„To be aware“ ist Englisch für „aufmerksam / bewusst sein“. Awarenessarbeit bedeutet, Leute zu sensibilisieren, auf sich selbst und andere zu achten - insbesondere auch auf jene, die gesellschaftlichen Erwartungshaltungen nicht entsprechen. Diskriminierung und Grenzüberschreitungen sind häufig den Ausübenden nicht bewusst, den Betroffenen sehr wohl. Da die Bedürfnisse Betroffener oft unsichtbar bleiben, wollen wir sie besonders unterstützen, diese Bedürfnisse ernst zu nehmen und ihnen Geltung zu verschaffen. Auch was Andere vielleicht als "vermeindliche Kleinigkeiten" bezeichnen oder wegzureden versuchen, können verletzen und beeinträchtigen und sind es daher wert, problematisiert zu werden.

Ein wichtiger Anteil von Awareness ist die Anerkennung der Definitionsmacht: Ob ihre eigenen Grenzen überschritten wurden, entscheidet immer ausschließlich die betroffene Person. Sonst nimmt sich oft die Mehrheitsgesellschaft das Recht heraus zu entscheiden, wer wann diskriminiert wird und wer nicht. Uns ist wichtig, dass auch Menschen wahrgenommen werden, die sonst nicht gehört oder ernst genommen werden.

Für uns bedeutet das: Wir unterstützen Betroffene einerseits, erlebte Situationen selbst zu verarbeiten. Wir bieten einen Ort für vertrauliche Gespräche. Wir hören zu und unterstützen ggf. dabei, Umgangsmöglichkeiten zu entwickeln und damit Handlungsfähigkeit zu stärken.

Andererseits kommunizieren wir auf Wunsch mit den Personen, von denen Diskriminierung und grenzüberschreitendes Verhalten ausgeht oder ausging. Dabei ist das Ziel die benannte Awareness: Verständnis und Bewusstsein dafür zu erzeugen, wie und warum das eigene Verhalten die Grenzen einer anderen Person verletzt hat. Wir treten dem übergriffigen oder verletzenden Verhalten entgegen, nicht dem Menschen. Ziel ist es, Verantwortung für das eigene Verhalten zu übernehmen.

Wenn es um die Bearbeitung von Konflikten geht, stehen wir auch für Moderation oder Begleitung zur Verfügung.

## Wie wir arbeiten

Das Awarenessteam steht euch insbesondere zur Seite, wenn:

- eure persönlichen Grenzen missachtet wurden,
- ihr belästigt oder diskriminiert wurdet,
- ihr (persönliche oder strukturelle) Diskriminierung mitbekommt,
- ihr euch unwohl fühlt und eine Person zum Reden braucht.

Wenn ihr euch an uns wendet, fragen wir euch in der Regel zuerst, was passiert
ist und was ihr gerade braucht. Vielleicht habt ihr bereits konkrete Ideen, was
wir tun können. Je nachdem, was ihr wünscht, können wir zum Beispiel:

- bei Konfliktsituationen auf dem Event zu euch kommen und vermitteln.
- einen ruhigen Raum bieten, in den ihr euch erst einmal (alleine oder mit uns) zurückziehen und das Erlebte verarbeiten könnt.
- Freund:innen und Bekannten kontaktieren.
- versuchen, eine belastende Situation (soweit möglich) zu klären (bspw. auch, indem wir grenzüberschreitenden Personen darzulegen versuchen, wieso ihr Handeln grenzüberschreitend war).
- gemeinsam mit euch versuchen herauszufinden, was euch am besten helfen könnte.

Welches Vorgehen für euch das beste ist, entscheidet immer ihr selbst - wir
werden nichts gegen euren Willen tun. Es ist in Ordnung, wenn ihr über etwas
nicht reden wollt oder könnt. Jede Reaktion, ob Trauer, Wut, Verzweiflung,
Sprachlosigkeit, ... ist okay. 

Disclaimer: Das Awarenessteam besteht aus Menschen, die für Awareness
sensibilisiert sind und in verschiedenen Bereichen Erfahrung haben. Wir
unterstützen euch so gut wir es können. Allerdings möchten wir explizit darauf
hinweisen, dass wir keine therapeutische Ausbildung haben.

Auf ein wunderbares Chaos-Event! Passt auf euch und aufeinander auf.
Euer Awareness-Team

## Mitmachen

Wir suchen noch Menschen, die sich vorstellen können, während dem Quellcode
Awareness-Schichten zu übernehmen. Meldet euch bitte per E-Mail bei
awareness@schleuder.0x90.space, wenn ihr Interesse habt.

Was wir uns von Interessierten wünschen:

- Selbsterfahrung und Reflexion (Du kennst dich und deine Grenzen)
- Selbstorganisation und Zuverlässigkeit
- Team- und Konfliktfähigkeit
- Einfühlungsvermögen
- Lernbereitschaft
- Offenheit und die Fähigkeit zuzuhören
- Diskriminierungssensibilität inklusive Anerkennung von struktureller Diskriminierung

Wir freuen uns, dich kennenzulernen.

