---
title: "Contact"
date: 2023-07-26T10:28:10+02:00
---

You can reach us over:

- Email: [people@schleuder.0x90.space](mailto:people@schleuder.0x90.space)
- Matrix: [https://matrix.to/#/#quellcode:chat.heizhaus.org](https://matrix.to/#/#quellcode:chat.heizhaus.org)
- Imprint: [here](https://0x90.space/impressum/)
