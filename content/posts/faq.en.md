---
title: "FAQ"
ShowToc: True
date: 2023-07-26T10:28:10+02:00
---

### Where can I stay for the night?

We have an [accommodation exchange](https://cryptpad.fr/code/#/2/code/edit/tObkSeg6EaOLiLNwUxsU5flt/) where you can offer and search for accommodation.

### How can I help? (shift schedule)

We have a [shift plan](https://cryptpad.fr/sheet/#/2/sheet/edit/o00OoL20ICKVtA1hL+KbAZyp/) and we are still looking for many helpers :)

### How do I get to the event location (Heizhaus)?
Address:
Heizhaus, Wandererstrasse 89, 90431 Nuremberg

With the Metro:
The event location is near the subway station Eberhardshof. When you get there, it is best to go out of the station in the direction of the "Netto" supermarket (you can see it in the distance) and then pass it on the right.
If you see the big tower, then the event location is directly below.

### Are there Corona Measures in Place?

Of course.
It's not long:

1. In the workshop room everyone wears masks,
   so the presenter doesn't have to.
   We bought some masks
   for anyone who hasn't brought one anyway.
2. In the hackcenter we open two of the rolling doors
   during the day.
   If we have to close them for safety reasons,
   we will let air in
   once an hour.
   Between 22:00 and 06:00 we need to close them unfortunately.

### Is There an Awareness Concept for the Event?

Of course,
you can find it [on this page](../awareness).

We are still looking for people
who can imagine taking a shift during the event;
please come to the awareness team meeting
Saturday at 11:00 in front of the rolling doors
to participate,
or send a mail to awareness@schleuder.0x90.space.

### I have a problem, who can I contact?

The quickest way to reach us is via the Matrix channel: https://matrix.to/#/#quellcode:chat.heizhaus.org

There will be a responsible person on site ("running information desk"), who you will recognize by a safety vest or similar. You can contact them at any time.
