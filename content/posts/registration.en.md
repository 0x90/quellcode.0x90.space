---
title: "Registration"
date: 2023-07-26T10:22:25+02:00
---

We want to estimate how many people will come and how much food we have to prepare, that is why we kindly ask you to fill out this registration form.

[Registration form](https://cryptpad.fr/form/#/2/form/view/qv48AVyeLO4-l9uIuTOeNdM7sGFR-zfTilvA8sX3-BY/)

Of course you can also come without registration, but don't eat too much ;) 
