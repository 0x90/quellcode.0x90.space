set -xe
hugo
cd ../0x90-ansible
rsync -avz -e 'ssh -F ssh_config' ../quellcode.0x90.space/public/* www:/var/www/quellcode.0x90.space
