# quellcode.0x90.space


## Setup development environment

```
git clone https://git.0x90.space/0x90/quellcode.0x90.space

cd quellcode.0x90.space/

hugo server
```

## Deployment

Assuming you are in the root directory of this repository, and you have cloned
https://git.0x90.space/0x90/0x90-ansible next to this repository, you can
deploy the website with:

```
# build website
hugo
cd ../0x90-ansible/
# create ssh_config (only necessary once)
ansible-playbook -i inventory -t init site.yml
# add key (only necessary once)
ssh-add id_ed25519
# copy website to server
rsync -avz -e 'ssh -F ssh_config' ../quellcode.0x90.space/public/* www:/var/www/quellcode.0x90.space
```

## Theme

We use the [PaperMod Theme](https://github.com/adityatelange/hugo-PaperMod).
Documentation is there.

To update the theme, you need to run these git commands:

```
git remote add papermod-hugo-theme https://github.com/adityatelange/hugo-PaperMod
git subtree pull --prefix themes/PaperMod papermod-hugo-theme master --squash
```

This adds the changes as a commit, and another merge commit. Now you can push
and deploy the updated theme.

